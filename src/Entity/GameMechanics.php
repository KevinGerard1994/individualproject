<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameMechanicsRepository")
 */
class GameMechanics
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $items;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $magics;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $equipment;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $abilities;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $enemies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $boss;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItems(): ?string
    {
        return $this->items;
    }

    public function setItems(string $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function getMagics(): ?string
    {
        return $this->magics;
    }

    public function setMagics(string $magics): self
    {
        $this->magics = $magics;

        return $this;
    }

    public function getEquipment(): ?string
    {
        return $this->equipment;
    }

    public function setEquipment(string $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getAbilities(): ?string
    {
        return $this->abilities;
    }

    public function setAbilities(string $abilities): self
    {
        $this->abilities = $abilities;

        return $this;
    }

    public function getEnemies(): ?string
    {
        return $this->enemies;
    }

    public function setEnemies(string $enemies): self
    {
        $this->enemies = $enemies;

        return $this;
    }

    public function getBoss(): ?string
    {
        return $this->boss;
    }

    public function setBoss(string $boss): self
    {
        $this->boss = $boss;

        return $this;
    }
}
