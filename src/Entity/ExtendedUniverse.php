<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExtendedUniverseRepository")
 */
class ExtendedUniverse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fanfictions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $backstory;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $chronology;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $movies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $files;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFanfictions(): ?string
    {
        return $this->fanfictions;
    }

    public function setFanfictions(string $fanfictions): self
    {
        $this->fanfictions = $fanfictions;

        return $this;
    }

    public function getBackstory(): ?string
    {
        return $this->backstory;
    }

    public function setBackstory(string $backstory): self
    {
        $this->backstory = $backstory;

        return $this;
    }

    public function getChronology(): ?string
    {
        return $this->chronology;
    }

    public function setChronology(string $chronology): self
    {
        $this->chronology = $chronology;

        return $this;
    }

    public function getMovies(): ?string
    {
        return $this->movies;
    }

    public function setMovies(string $movies): self
    {
        $this->movies = $movies;

        return $this;
    }

    public function getFiles(): ?string
    {
        return $this->files;
    }

    public function setFiles(string $files): self
    {
        $this->files = $files;

        return $this;
    }
}
