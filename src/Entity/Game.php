<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $characters;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bestiary;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $walkthroughs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sideQuests;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tips;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $locations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCharacters(): ?string
    {
        return $this->characters;
    }

    public function setCharacters(string $characters): self
    {
        $this->characters = $characters;

        return $this;
    }

    public function getBestiary(): ?string
    {
        return $this->bestiary;
    }

    public function setBestiary(string $bestiary): self
    {
        $this->bestiary = $bestiary;

        return $this;
    }

    public function getWalkthroughs(): ?string
    {
        return $this->walkthroughs;
    }

    public function setWalkthroughs(string $walkthroughs): self
    {
        $this->walkthroughs = $walkthroughs;

        return $this;
    }

    public function getSideQuests(): ?string
    {
        return $this->sideQuests;
    }

    public function setSideQuests(string $sideQuests): self
    {
        $this->sideQuests = $sideQuests;

        return $this;
    }

    public function getTips(): ?string
    {
        return $this->tips;
    }

    public function setTips(string $tips): self
    {
        $this->tips = $tips;

        return $this;
    }

    public function getLocations(): ?string
    {
        return $this->locations;
    }

    public function setLocations(string $locations): self
    {
        $this->locations = $locations;

        return $this;
    }
}
