<?php

namespace App\Repository;

use App\Entity\ExtendedUniverse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtendedUniverse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtendedUniverse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtendedUniverse[]    findAll()
 * @method ExtendedUniverse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtendedUniverseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtendedUniverse::class);
    }

    // /**
    //  * @return ExtendedUniverse[] Returns an array of ExtendedUniverse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtendedUniverse
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
