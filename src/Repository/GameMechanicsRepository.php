<?php

namespace App\Repository;

use App\Entity\GameMechanics;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameMechanics|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameMechanics|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameMechanics[]    findAll()
 * @method GameMechanics[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameMechanicsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameMechanics::class);
    }

    // /**
    //  * @return GameMechanics[] Returns an array of GameMechanics objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameMechanics
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
